# README #

This is an Android app written in the Kotlin programming language. It's primary goal is for the developer (Harsh) to learn how to use the Kotlin programming language.

WatchSomething picks a random episode of a TV Show that you select and attempts to play that episode on the associated support app (HBO GO, Hulu, etc.). The app is a work in progress and is not complete yet.