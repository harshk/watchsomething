package io.peaklabs.watchsomething.di

import dagger.Component
import io.peaklabs.watchsomething.ui.mainscreen.MainActivity
import io.peaklabs.watchsomething.di.AppModule
import io.peaklabs.watchsomething.ui.mainscreen.MainActivityPresenter
import io.peaklabs.watchsomething.ui.randomscreen.RandomEpisodeDialogPresenter
import javax.inject.Singleton

/**
 * Created by harshk on 1/11/17.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(activity: MainActivity)
    fun inject(presenter: MainActivityPresenter)
    fun inject(presenter: RandomEpisodeDialogPresenter)

}