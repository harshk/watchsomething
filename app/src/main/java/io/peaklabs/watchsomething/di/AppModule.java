package io.peaklabs.watchsomething.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.peaklabs.watchsomething.BuildConfig;
import io.peaklabs.watchsomething.net.GuideboxApi;
import io.peaklabs.watchsomething.util.ContextPresenterImpl;
import io.peaklabs.watchsomething.util.ContextPresenter;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by harshk on 1/11/17.
 */

@Module
public class AppModule {

    private static final Interceptor headerInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("Authorization", BuildConfig.GUIDEBOX_API_KEY)
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        }
    };

    @Provides
    @Singleton
    Retrofit provideGuideboxRetrofit() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(headerInterceptor)
                .build();

        Gson gson = new GsonBuilder().setLenient().create();

        return new Retrofit.Builder()
                .baseUrl("http://api-public.guidebox.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    GuideboxApi provideGuideboxApi() {
        return provideGuideboxRetrofit().create(GuideboxApi.class);
    }

    @Provides
    @Singleton
    ContextPresenter provideContextPresenter() {
        return new ContextPresenterImpl();
    }

}
