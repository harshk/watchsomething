package io.peaklabs.watchsomething.ui.mainscreen

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import io.peaklabs.watchsomething.R
import io.peaklabs.watchsomething.net.model.Show
import io.peaklabs.watchsomething.ui.randomscreen.RandomEpisodeDialog
import kotlinx.android.synthetic.main.view_show.view.*

/**
 * Created by harshk on 1/15/17.
 */

class ShowsAdapter (context: Context, items: MutableList<Show>) : RecyclerView.Adapter<ShowsAdapter.ViewHolder>() {

    val context: Context = context
    var items: MutableList<Show> = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder? {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_show, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val show = items.get(position)
        Glide.with(context).load(show.artwork608x342).into(holder.showImage)
        holder.layout.setOnClickListener {
            Toast.makeText(context, "Clicked on : ${show.title}", Toast.LENGTH_LONG).show()
            var dialog: RandomEpisodeDialog = RandomEpisodeDialog(context, show)
            dialog.show()
        }
    }

    fun addItems(newItems: List<Show>) {
        items.addAll(newItems)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val layout = v.show_layout
        val showImage = v.show_image
    }
}
