package io.peaklabs.watchsomething.ui.randomscreen

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import io.peaklabs.watchsomething.R
import io.peaklabs.watchsomething.net.model.Episode
import io.peaklabs.watchsomething.net.model.EpisodeDetails
import io.peaklabs.watchsomething.net.model.Show
import kotlinx.android.synthetic.main.view_show.*
import org.jetbrains.anko.onClick
import android.content.pm.ResolveInfo
import android.util.Log


/**
 * Created by harshk on 1/16/17.
 */
class RandomEpisodeDialog(context: Context, show: Show) : AlertDialog(context),
        RandomEpisodeDialogContract.Ui {

    /*
    var show: Show? = null
    constructor (context: Context, show: Show) : this(context) {
        this.show = show
    }
    */
    var show: Show? = null
    init {
        this.show = show
    }

    var presenter: RandomEpisodeDialogPresenter = RandomEpisodeDialogPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_random_episode)

        presenter.loadRandomEpisode(show?.id ?: -1)
    }

    override fun onRandomEpisodeLoaded(episode: Episode) {
        var showNameTv = findViewById(R.id.dialog_show_name) as TextView
        var episodeTitleTv = findViewById(R.id.dialog_episode_name) as TextView
        var seasonAndEpisodeTv = findViewById(R.id.dialog_season_and_episode_num) as TextView
        var originalAirDateTv = findViewById(R.id.dialog_original_airdate) as TextView
        var imageViewTv = findViewById(R.id.dialog_episode_image) as ImageView
        var favoritesBtn = findViewById(R.id.dialog_favorites_btn) as Button
        var playNowBtn = findViewById(R.id.dialog_play_now_btn) as Button
        var loadDifferentEpisodeBtn = findViewById(R.id.dialog_load_different_episode_btn) as Button

        showNameTv.text = show?.title
        episodeTitleTv.text = episode.title
        seasonAndEpisodeTv.text = "Season ${episode.seasonNumber}, Episode ${episode.episodeNumber}"
        originalAirDateTv.text = episode.firstAired
        Glide.with(context).load(episode.thumbnail608x342).into(imageViewTv)

        playNowBtn.onClick {
            //presenter.playNow(episode)
            presenter.loadEpisodeDetailsThenPlay(episode.id ?: -1)
        }

        loadDifferentEpisodeBtn.onClick { presenter.loadRandomEpisode(show?.id ?: -1) }

        favoritesBtn.onClick {

        }

    }

    override fun playEpisode(deepLinkUri: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(deepLinkUri)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    override fun showToast(msg: String) {
    }

    override fun showErrorUnableToLoadShow(msg: String) {
    }

    override fun showErrorUnknownError(msg: String) {
    }
}