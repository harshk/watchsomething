package io.peaklabs.watchsomething.ui.mainscreen

import io.peaklabs.watchsomething.net.model.Show

/**
 * Created by harshk on 1/12/17.
 */
interface MainActivityContract {
    interface Presenter {
        fun sendRequestGetFavoriteShows(favoriteShowIds: Set<Int>)
        fun sendRequestGetTopShows()
        fun rebind(ui: MainActivityContract.Ui)
        fun onResume()
    }

    interface Ui {
        fun showToast(msg: String)
        fun onLoadedTopShows(topShows: List<Show>)
    }
}