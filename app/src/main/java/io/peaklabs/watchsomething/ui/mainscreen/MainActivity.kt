package io.peaklabs.watchsomething.ui.mainscreen

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import io.peaklabs.watchsomething.PresenterCache
import io.peaklabs.watchsomething.R
import io.peaklabs.watchsomething.WatchSomethingApp
import io.peaklabs.watchsomething.net.GuideboxApi
import io.peaklabs.watchsomething.net.model.Show
import io.peaklabs.watchsomething.ui.common.FavoritesContract
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(),
        MainActivityContract.Ui, FavoritesContract.Ui {

    val TAG = "MainActivity"
    @Inject lateinit var guideBoxApi: GuideboxApi
    var mainActivityPresenter: MainActivityPresenter? = null

    var showsAdapter: ShowsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        WatchSomethingApp.appComponent.inject(this)

        if (savedInstanceState == null) {
            mainActivityPresenter = MainActivityPresenter(this)
        } else {
            mainActivityPresenter = PresenterCache.getInstance().getPresenter(TAG) as MainActivityPresenter
            mainActivityPresenter?.rebind(this)
        }

        val toolbar = findViewById(R.id.toolbar) as Toolbar

        showsAdapter = ShowsAdapter(this, ArrayList<Show>())
        val showRecyclerView = findViewById(R.id.shows_rv) as RecyclerView
        showRecyclerView.layoutManager = GridLayoutManager(this, 2)

        showRecyclerView.adapter = showsAdapter

        setSupportActionBar(toolbar)

        mainActivityPresenter?.sendRequestGetTopShows()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        PresenterCache.getInstance().addPresenter(TAG, mainActivityPresenter)
    }

    override fun onResume() {
        super.onResume()
        mainActivityPresenter?.onResume()
    }

    override fun showToast(msg: String) {
        Toast.makeText(this@MainActivity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onLoadedTopShows(topShows: List<Show>) {
        Log.d("MainActivity", "onLoadedTopShows 1.0")
        showsAdapter?.items = topShows.toMutableList()
        showsAdapter?.notifyDataSetChanged()
        Log.d("MainActivity", "onLoadedTopShows 2.0 : showsAdapter.items.size = " + showsAdapter?.items?.size)
    }

    override fun favoriteShowsLoaded(favoriteShows: Set<Int>) {
    }

    override fun favoriteShowRemoved(showId: Int) {
    }

    override fun favoriteShowAdded(showId: Int) {
    }
}
