package io.peaklabs.watchsomething.ui.common

/**
 * Created by harshk on 1/14/17.
 */
interface FavoritesContract {

    interface Presenter {
        fun clearFavoriteShows()
        fun loadFavoriteShows()
        fun addFavoriteShow(showId: Int)
        fun removeFavoriteShow(showId: Int)
    }

    interface Ui {
        fun favoriteShowsLoaded(favoriteShows: Set<Int>)
        fun favoriteShowRemoved(showId: Int)
        fun favoriteShowAdded(showId: Int)
    }
}