package io.peaklabs.watchsomething.ui.randomscreen

import io.peaklabs.watchsomething.net.model.Episode
import io.peaklabs.watchsomething.net.model.EpisodeDetails

/**
 * Created by harshk on 1/14/17.
 */

interface RandomEpisodeDialogContract {

    interface Presenter {
        fun loadRandomEpisode(showId: Int)
        fun loadEpisodeDetailsThenPlay(episodeId: Int)
    }

    interface Ui {
        fun onRandomEpisodeLoaded(episode: Episode)
        fun playEpisode(deepLinkUri: String)
        fun showToast(msg: String)
        fun showErrorUnableToLoadShow(msg: String)
        fun showErrorUnknownError(msg: String)
    }
}