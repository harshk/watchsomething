package io.peaklabs.watchsomething.ui.randomscreen

import android.util.Log
import io.peaklabs.watchsomething.WatchSomethingApp
import io.peaklabs.watchsomething.net.GuideboxApi
import io.peaklabs.watchsomething.net.model.*
import io.peaklabs.watchsomething.util.ContextPresenter
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.ref.WeakReference
import java.util.*
import javax.inject.Inject

/**
 * Created by harshk on 1/14/17.
 */
class RandomEpisodeDialogPresenter (ui: RandomEpisodeDialogContract.Ui) : RandomEpisodeDialogContract.Presenter {

    @Inject lateinit var guideBoxApi: GuideboxApi
    @Inject lateinit var contextPresenter: ContextPresenter

    val TAG = "RandomEpisodeDlgPrsntr"
    var ui: WeakReference<RandomEpisodeDialogContract.Ui>? = null

    init {
        WatchSomethingApp.appComponent.inject(this)
        this.ui = WeakReference(ui)
    }

    override fun loadRandomEpisode(showId: Int) {
        val random = Random()
        guideBoxApi
                .numSeasonsObservable(showId)
                .flatMap { seasons ->
                    val randomSeason = random.nextInt(seasons.totalResults ?: 0) + 1
                    println("RANDOMLY SELECTED SEASON = ${randomSeason}")
                    guideBoxApi.episodesObservable(showId, randomSeason)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            episodes ->
                                if (episodes.results?.size ?: 0 > 0) {
                                    var allEpisodes = episodes.results
                                    val seasonNum = allEpisodes?.get(0)?.seasonNumber
                                    var filteredEpisodes = allEpisodes?.filter { ep -> ep.seasonNumber == seasonNum }
                                    val episode = filteredEpisodes?.get(random.nextInt(filteredEpisodes.size))
                                    if (episode != null) {
                                        Log.d("TAG", "EPISODE NUM ${episode.episodeNumber}, TITLE: ${episode.title}")
                                        ui?.get()?.onRandomEpisodeLoaded(episode)
                                    } else {
                                        // TODO: handle error
                                        Log.e("TAG", "error 1.0")
                                    }
                                } else {
                                    // TODO: handle error
                                    Log.e("TAG", "error 2.0")
                                }
                        },
                        {
                            exc ->
                                Log.e("TAG", "Exception thrown while trying to get episodes")
                                println(exc)
                        }
                )
    }

    override fun loadEpisodeDetailsThenPlay(episodeId: Int) {
        guideBoxApi
                .episodeDetailsObservable(episodeId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            episodeDetails ->
                                var newList: MutableList<Source> = mutableListOf()
                                episodeDetails.subscriptionAndroidSources?.forEach { newList.add(it) }
                                episodeDetails.tvEverywhereAndroidSources?.forEach { newList.add(it) }

                                Log.d(TAG, "all sources = " + WatchSomethingApp.supportedSources)
                                newList.forEach { Log.d(TAG, "1st: ${it.source} -> WatchSomethingApp.supportedSources.contains(${it.source}) == " + WatchSomethingApp.supportedSources.contains(it.source))  }
                                newList = episodeDetails.subscriptionAndroidSources?.filter {
                                    WatchSomethingApp.supportedSources.contains(it.source)
                                } as MutableList<Source>
                                newList.forEach { Log.d(TAG, "2nd: ${it.source}")  }

                                var link = getLink(newList)
                                Log.d(TAG, "THE LINK IS : " + link)
                                if (link != null && link.isNotBlank()) {
                                    ui?.get()?.playEpisode(link)
                                } else {
                                    ui?.get()?.showErrorUnableToLoadShow("could not find any apps on your device that can play this episode")
                                }
                        },
                        {
                            exc ->
                            Log.e(TAG, "Exception thrown while trying to get episode Details", exc)
                            ui?.get()?.showErrorUnknownError("Exception thrown while trying to get episode Details")
                        }

                )
    }

    private fun getLink(sources: List<Source>) : String {

        for (item in sources) {
            if (item.appDownloadLink?.contains("play.google.com") ?: false &&
                    getAppPackageNameFromStoreLink(item.appDownloadLink ?: "HMM THERES NO LINK") in getInstalledApps()) {
                return item.link ?: ""
            }
        }

        return ""
    }

    private fun getAppPackageNameFromStoreLink(storeLink: String): String {
        var parts = storeLink.split("=")
        if (parts != null && parts.size == 2 && parts.get(0).contains("play.google.com")) {
            Log.d(TAG, "Store Link: ${storeLink}, Package Name: ${parts.get(1)}")
            return parts.get(1)
        }
        return ""
    }

    private fun getInstalledApps(): List<String> {

        var l = contextPresenter.getInstalledPackages()
        l.forEach {
            println("installed packages :  : "  + it)
        }

        return l
    }
}