package io.peaklabs.watchsomething.ui.mainscreen

import android.util.Log
import io.peaklabs.watchsomething.WatchSomethingApp
import io.peaklabs.watchsomething.net.GuideboxApi
import io.peaklabs.watchsomething.net.model.Show
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.lang.ref.WeakReference
import java.util.*
import javax.inject.Inject

/**
 * Created by harshk on 1/12/17.
 */
class MainActivityPresenter(ui: MainActivityContract.Ui) : MainActivityContract.Presenter {

    val TAG = "MainActivityPresenter"

    @Inject lateinit var guideBoxApi: GuideboxApi
    var ui: WeakReference<MainActivityContract.Ui> ?= null
    var topShows: List<Show>? = null

    init {
        this.ui = WeakReference(ui)
        WatchSomethingApp.appComponent.inject(this)
    }

    override fun rebind(ui: MainActivityContract.Ui) {
        this.ui = WeakReference(ui)
    }

    override fun onResume() {
        Log.d(TAG, "onResume() called .. topshows.length = ${topShows?.size}")
        ui?.get()?.onLoadedTopShows(topShows ?: ArrayList<Show>())
    }

    override fun sendRequestGetTopShows() {
        async {
            val showsRsp = guideBoxApi.shows().execute()
            if (showsRsp.isSuccessful) {
                val shows = showsRsp.body()
                topShows = shows.results
                uiThread {
                    ui?.get()?.onLoadedTopShows(topShows ?: ArrayList<Show>())
                }
            }
        }
    }

    override fun sendRequestGetFavoriteShows(favoriteShowIds: Set<Int>) {

    }

}