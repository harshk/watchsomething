package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Show {

    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("title")
    @Expose
    var title: String? = null

    @SerializedName("alternate_titles")
    @Expose
    var alternateTitles: List<String>? = null

    @SerializedName("container_show")
    @Expose
    var containerShow: Int? = null

    @SerializedName("first_aired")
    @Expose
    var firstAired: String? = null

    @SerializedName("imdb_id")
    @Expose
    var imdbId: String? = null

    @SerializedName("tvdb")
    @Expose
    var tvdb: Int? = null

    @SerializedName("themoviedb")
    @Expose
    var themoviedb: Int? = null

    @SerializedName("freebase")
    @Expose
    var freebase: String? = null

    @SerializedName("wikipedia_id")
    @Expose
    var wikipediaId: Int? = null

    @SerializedName("tvrage")
    @Expose
    var tvrage: Tvrage? = null

    @SerializedName("artwork_208x117")
    @Expose
    var artwork208x117: String? = null

    @SerializedName("artwork_304x171")
    @Expose
    var artwork304x171: String? = null

    @SerializedName("artwork_448x252")
    @Expose
    var artwork448x252: String? = null

    @SerializedName("artwork_608x342")
    @Expose
    var artwork608x342: String? = null

}