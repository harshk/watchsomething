package io.peaklabs.watchsomething.net

import io.peaklabs.watchsomething.net.model.EpisodeDetails
import io.peaklabs.watchsomething.net.model.Episodes
import io.peaklabs.watchsomething.net.model.Shows
import io.peaklabs.watchsomething.net.models.Seasons
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

/**
 * Created by harshk on 1/11/17.
 */

interface GuideboxApi {

    @GET("/v2/shows?limit=150&sources=netflix")
    fun shows() : Call<Shows>

    @GET("/v2/shows?limit=150&sources=netflix")
    fun showsObservable() : Observable<Shows>

    @GET("/v2/shows/{showId}/seasons")
    fun numSeasons(@Path("showId") showId: Int) : Call<Seasons>

    @GET("/v2/shows/{showId}/seasons")
    fun numSeasonsObservable(@Path("showId") showId: Int) : Observable<Seasons>

    @GET("/v2/shows/{showId}/episodes")
    fun episodes(@Path("showId") showId: Int, @Query("season") season: Int?) : Call<Episodes>

    @GET("/v2/shows/{showId}/episodes")
    fun episodesObservable(@Path("showId") showId: Int, @Query("season") season: Int?) : Observable<Episodes>

    @GET("/v2/episodes/{episodeId}?platform=android&sources=netflix")
    fun episodeDetails(@Path("episodeId") episodeId: Int) : Call<EpisodeDetails>

    @GET("/v2/episodes/{episodeId}?platform=android&sources=netflix")
    fun episodeDetailsObservable(@Path("episodeId") episodeId: Int) : Observable<EpisodeDetails>
}
