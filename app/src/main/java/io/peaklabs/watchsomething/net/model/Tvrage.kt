package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Tvrage {

    @SerializedName("tvrage_id")
    @Expose
    var tvrageId: Int? = null

    @SerializedName("link")
    @Expose
    var link: String? = null

}