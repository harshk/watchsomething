package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Source {

    @SerializedName("source")
    @Expose
    var source: String? = null
    @SerializedName("display_name")
    @Expose
    var displayName: String? = null
    @SerializedName("tv_channel")
    @Expose
    var tvChannel: String? = null
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("link")
    @Expose
    var link: String? = null
    @SerializedName("app_name")
    @Expose
    var appName: String? = null
    @SerializedName("app_link")
    @Expose
    var appLink: Int? = null
    @SerializedName("app_required")
    @Expose
    var appRequired: Int? = null
    @SerializedName("app_download_link")
    @Expose
    var appDownloadLink: String? = null
    @SerializedName("formats")
    @Expose
    var formats: List<Format>? = null

}