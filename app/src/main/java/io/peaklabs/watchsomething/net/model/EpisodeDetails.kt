package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EpisodeDetails {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("tvdb")
    @Expose
    var tvdb: Int? = null
    @SerializedName("content_type")
    @Expose
    var contentType: String? = null
    @SerializedName("is_shadow")
    @Expose
    var isShadow: Int? = null
    @SerializedName("alternate_tvdb")
    @Expose
    var alternateTvdb: List<Any>? = null
    @SerializedName("imdb_id")
    @Expose
    var imdbId: String? = null
    @SerializedName("themoviedb")
    @Expose
    var themoviedb: Int? = null
    @SerializedName("show_id")
    @Expose
    var showId: Int? = null
    @SerializedName("season_number")
    @Expose
    var seasonNumber: Int? = null
    @SerializedName("episode_number")
    @Expose
    var episodeNumber: Int? = null
    @SerializedName("special")
    @Expose
    var special: Int? = null
    @SerializedName("first_aired")
    @Expose
    var firstAired: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("original_title")
    @Expose
    var originalTitle: String? = null
    @SerializedName("alternate_titles")
    @Expose
    var alternateTitles: List<Any>? = null
    @SerializedName("tags")
    @Expose
    var tags: List<Tag>? = null
    @SerializedName("overview")
    @Expose
    var overview: String? = null
    @SerializedName("duration")
    @Expose
    var duration: Int? = null
    @SerializedName("production_code")
    @Expose
    var productionCode: String? = null
    @SerializedName("writers")
    @Expose
    var writers: List<Any>? = null
    @SerializedName("directors")
    @Expose
    var directors: List<Any>? = null
    @SerializedName("cast")
    @Expose
    var cast: List<Cast>? = null
    @SerializedName("guest_stars")
    @Expose
    var guestStars: List<Any>? = null
    @SerializedName("thumbnail_208x117")
    @Expose
    var thumbnail208x117: String? = null
    @SerializedName("thumbnail_304x171")
    @Expose
    var thumbnail304x171: String? = null
    @SerializedName("thumbnail_400x225")
    @Expose
    var thumbnail400x225: String? = null
    @SerializedName("thumbnail_608x342")
    @Expose
    var thumbnail608x342: String? = null
    @SerializedName("free_web_sources")
    @Expose
    var freeWebSources: List<Any>? = null
    @SerializedName("free_ios_sources")
    @Expose
    var freeIosSources: List<Any>? = null
    @SerializedName("free_android_sources")
    @Expose
    var freeAndroidSources: List<Any>? = null
    @SerializedName("tv_everywhere_web_sources")
    @Expose
    var tvEverywhereWebSources: List<Source>? = null
    @SerializedName("tv_everywhere_ios_sources")
    @Expose
    var tvEverywhereIosSources: List<Source>? = null
    @SerializedName("tv_everywhere_android_sources")
    @Expose
    var tvEverywhereAndroidSources: List<Source>? = null
    @SerializedName("subscription_web_sources")
    @Expose
    var subscriptionWebSources: List<Source>? = null
    @SerializedName("subscription_ios_sources")
    @Expose
    var subscriptionIosSources: List<Source>? = null
    @SerializedName("subscription_android_sources")
    @Expose
    var subscriptionAndroidSources: List<Source>? = null
    @SerializedName("purchase_web_sources")
    @Expose
    var purchaseWebSources: List<Source>? = null
    @SerializedName("purchase_ios_sources")
    @Expose
    var purchaseIosSources: List<Source>? = null
    @SerializedName("purchase_android_sources")
    @Expose
    var purchaseAndroidSources: List<Source>? = null

}