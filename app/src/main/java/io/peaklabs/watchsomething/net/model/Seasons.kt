package io.peaklabs.watchsomething.net.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import io.peaklabs.watchsomething.net.model.Season

class Seasons {

    @SerializedName("results")
    @Expose
    var results: List<Season>? = null

    @SerializedName("total_results")
    @Expose
    var totalResults: Int? = null

}