package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Format {

    @SerializedName("price")
    @Expose
    var price: String? = null
    @SerializedName("format")
    @Expose
    var format: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null

}