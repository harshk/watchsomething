package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Episodes {

    @SerializedName("total_results")
    @Expose
    var totalResults: Int? = null

    @SerializedName("total_returned")
    @Expose
    var totalReturned: Int? = null

    @SerializedName("results")
    @Expose
    var results: List<Episode>? = null

}