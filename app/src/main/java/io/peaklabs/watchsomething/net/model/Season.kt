package io.peaklabs.watchsomething.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Season {

    @SerializedName("season_number")
    @Expose
    var seasonNumber: Int? = null

    @SerializedName("first_airdate")
    @Expose
    var firstAirdate: String? = null

}
