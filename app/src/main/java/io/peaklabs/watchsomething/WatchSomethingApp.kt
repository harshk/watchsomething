package io.peaklabs.watchsomething

import android.app.Application
import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs
import io.peaklabs.watchsomething.di.AppModule
import io.peaklabs.watchsomething.di.AppComponent
import io.peaklabs.watchsomething.di.DaggerAppComponent
import java.util.*

/**
 * Created by harshk on 1/12/17.
 */

class WatchSomethingApp: Application() {

    companion object {
        lateinit var instance: WatchSomethingApp
            private set

        lateinit var appComponent: AppComponent
            private set

        lateinit var supportedSources: MutableList<String>
            private set
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        appComponent = DaggerAppComponent.builder().appModule(AppModule()).build()

        Prefs.Builder().setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()

        supportedSources = mutableListOf("netflix", "hbo", "vudu", "hulu")
    }

}