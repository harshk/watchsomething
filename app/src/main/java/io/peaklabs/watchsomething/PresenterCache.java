package io.peaklabs.watchsomething;

import android.support.v4.util.SimpleArrayMap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by harshk on 1/16/17.
 */

/**
 * Not yet complete ...
 */
public class PresenterCache {

    private static PresenterCache instance = null;
    private Map<String, Object> presenters = new HashMap<>();

    private PresenterCache() {}

    public static PresenterCache getInstance() {
        if (instance == null) {
            instance = new PresenterCache();
        }
        return instance;
    }

    public Object getPresenter(String identifier) {
        return presenters.get(identifier);
    }

    public void addPresenter(String identifier, Object presenter) {
        presenters.put(identifier, presenter);
    }

    public Object removePresenter(String identifier) {
        return presenters.remove(identifier);
    }
}
