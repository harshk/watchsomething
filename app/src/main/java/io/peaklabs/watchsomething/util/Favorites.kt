package io.peaklabs.watchsomething.util

import com.pixplicity.easyprefs.library.Prefs
import java.util.*

/**
 * Created by harshk on 1/12/17.
 */
class Favorites {

    companion object instance {

        val FAVORITE_SHOWS_KEY = "favorite_shows"

        fun addShowId(showId: Int) {
            val favorites = Prefs.getOrderedStringSet(FAVORITE_SHOWS_KEY, HashSet<String>())
            favorites.add(showId.toString())
            Prefs.putOrderedStringSet(FAVORITE_SHOWS_KEY, favorites)
        }

        fun getShowIds(): Set<Int> {
            val favorites = Prefs.getOrderedStringSet(FAVORITE_SHOWS_KEY, HashSet<String>())
            val convertedFromStringsToInts = favorites
                    .map { str -> str.toInt() }

            if (convertedFromStringsToInts != null) {
                return HashSet<Int>(convertedFromStringsToInts)
            } else {
                return HashSet<Int>()
            }
        }

        fun clearFavoriteShowsIds() {
            Prefs.clear()
        }
    }

}