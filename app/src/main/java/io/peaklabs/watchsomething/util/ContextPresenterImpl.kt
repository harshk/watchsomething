package io.peaklabs.watchsomething.util

import android.content.pm.PackageManager
import android.util.Log

import java.util.ArrayList

import io.peaklabs.watchsomething.WatchSomethingApp

/**
 * Created by harshk on 1/17/17.
 */

class ContextPresenterImpl : ContextPresenter {

    override fun getInstalledPackages(): List<String> {

        val TAG = "ContextPresenterImpl"
        val packageNames: MutableList<String> = ArrayList<String>()

        val applicationInfoList = WatchSomethingApp.instance.packageManager.getInstalledApplications(PackageManager.GET_META_DATA)
        applicationInfoList?.forEach {
            packageNames.add(it.packageName)
            Log.d(TAG, "Installed package : " + it.packageName)
            Log.d(TAG, "Source dir        : " + it.sourceDir)
            Log.d(TAG, "Launch Activity   : " + WatchSomethingApp.instance.packageManager.getLaunchIntentForPackage(it.packageName))
            Log.d(TAG, "--------------------------------------------------------------------------------")
        }

        return packageNames
    }

}
