package io.peaklabs.watchsomething.util

/**
 * Created by harshk on 1/17/17.
 */
interface ContextPresenter {
    fun getInstalledPackages(): List<String>
}